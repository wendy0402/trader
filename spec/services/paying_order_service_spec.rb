describe PayingOrderService do
  let(:order) { FactoryGirl.create(:order, :submitted) }
  let(:draft_order) { FactoryGirl.create(:order) }
  describe 'execute' do
    let(:params) do
      {
        paid_at: Time.current.iso8601,
        account_number: '12343556778',
        account_name: 'joffrey',
        account_bank: 'bca'
      }
    end

    it 'create payment and update status to paid when success' do
      service = described_class.new(order, params)
      payment = service.execute
      expect(payment.errors).to be_blank
      expect(payment).to be_persisted
      expect(payment.paid_at).to eq(Time.zone.parse(params[:paid_at]))
      expect(payment.account_number).to eq(params[:account_number])
      expect(payment.account_name).to eq(params[:account_name])
      expect(payment.account_bank).to eq(params[:account_bank])
      expect(payment.order.reload.status).to eq(Order::PAID)
    end

    it 'dont create payment when payment is invalid' do
      params[:account_bank] = nil
      service = described_class.new(order, params)
      payment = service.execute
      expect(payment.errors).to be_present
      expect(payment).to_not be_persisted
    end

    it 'dont create payment when order_status is invalid' do
      params[:account_bank] = nil
      service = described_class.new(draft_order, params)
      payment = service.execute
      expect(payment.errors).to be_present
      expect(payment).to_not be_persisted
      expect(payment.errors[:order]).to eq(["can't be paid"])
    end
  end
end
