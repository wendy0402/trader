describe CancelOrderService do
  let(:order) { FactoryGirl.create(:order, :paid) }
  let(:draft_order) { FactoryGirl.create(:order) }
  describe 'execute' do
    it 'update status to cancel when success' do
      service = described_class.new(order)
      expect do
        service.execute
      end.to change{ order.reload.status}.from(Order::PAID).to(Order::CANCELLED)
    end

    it 'increase product quantity' do
      service = described_class.new(order)
      product = order.order_details[0].product
      expect do
        service.execute
      end.to change{ product.reload.quantity }.by(order.order_details[0].quantity)
    end

    it 'failed when order hasnt not paid' do
      service = described_class.new(draft_order)
      product = order.order_details[0].product
      expect do
        service.execute
      end.to_not change{ [order.reload.status, product.reload.quantity] }
    end
  end
end
