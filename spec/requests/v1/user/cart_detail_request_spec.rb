describe 'cart detail', type: :request do
  let(:product) { FactoryGirl.create(:product) }
  describe '#create' do
    it 'create order detail and return 201 when succeed' do
      expect do
        post '/v1/user/cart_details/', params: { quantity: 20, product_id: product.id }, headers: accept_json
      end.to change{ OrderDetail.count }.by(1)
      order_detail = OrderDetail.first
      expect(response.body).to eq(
        response_body(
          id: order_detail.id,
          product_id: order_detail.product_id,
          quantity: order_detail.quantity
        ).to_json
      )
      expect(response.status).to eq(201)
    end

    it 'dont create order detail when invalid' do
      expect do
        post '/v1/user/cart_details/', params: { quantity: -1, product_id: product.id }, headers: accept_json
      end.to_not change { OrderDetail.count }

      order_detail = OrderDetail.first

      expect(response.body).to eq(
        response_body(
          {},
          quantity: ["must be greater than 0"]
        ).to_json
      )
      expect(response.status).to eq(422)
    end
  end

  describe '#update' do
    let(:order) { FactoryGirl.create(:order) }
    let(:order_detail) { FactoryGirl.create(:order_detail, order: order, product: product) }

    it 'update order detail and return ok' do
      cookies['order_id'] = order.id
      request_details = {
        params: { quantity: 1020 },
        headers: accept_json
      }

      expect do
        put "/v1/user/cart_details/#{order_detail.id}", request_details
      end.to change { order_detail.reload.quantity }.to(1020)

      expect(response.body).to eq(
        response_body(
          id: order_detail.id,
          product_id: order_detail.product_id,
          quantity: order_detail.quantity
        ).to_json
      )
      expect(response.status).to eq(200)
    end

    it 'dont update order detail when invalid' do
      cookies['order_id'] = order_detail.order.id

      expect do
        put "/v1/user/cart_details/#{order_detail.id}", params: { quantity: -1 }, headers: accept_json
      end.to_not change { OrderDetail.count }

      order_detail = OrderDetail.first
      expect(response.body).to eq(
        response_body(
          {
            id: order_detail.id,
            product_id: order_detail.product_id,
            quantity: -1
          },
          quantity: ["must be greater than 0"]
        ).to_json
      )
      expect(response.status).to eq(422)
    end
  end
end
