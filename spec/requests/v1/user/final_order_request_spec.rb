describe 'final order', type: :request do
  let(:product) { FactoryGirl.create(:product) }
  let(:order) { FactoryGirl.create(:order) }
  let(:coupon) { FactoryGirl.create(:coupon) }
  let(:order_detail) { FactoryGirl.create(:order_detail, quantity: 2, order: order, product: product) }

  describe '#create' do
    it 'finalize order when valid' do
      cookies['order_id'] = order.id
      params = {
        name: 'joffrey',
        email: 'joffrey@kingslanding.com',
        address: 'beyond the wall',
        coupon_code: coupon.code
      }

      result = response_body(
        id: order.id,
        email: 'joffrey@kingslanding.com',
        name: 'joffrey',
        address: 'beyond the wall',
        amount: ((product.unit_price * 2) - coupon.value).to_f,
        coupon: {
          id: coupon.id,
          code: coupon.code,
          calculation_type: coupon.calculation_type,
          value: coupon.value
        },
        order_details: [
          product_id: order_detail.product_id,
          quantity: order_detail.quantity,
          amount: product.unit_price * 2
        ]
      )

      post '/v1/user/final_orders', params: params, headers: accept_json
      expect(response.body).to eq(result.to_json)
      expect(order.reload.status).to eq(Order::SUBMITTED)
      final_amount = (product.unit_price * 2) - coupon.value
      expect(order.amount).to eq(final_amount)
    end

    it 'dont finalize order when invalid' do
      cookies['order_id'] = order.id
      params = {
        email: 'joffrey@kingslanding.com',
        address: 'beyond the wall',
        coupon: coupon.code
      }
      post '/v1/user/final_orders', params: params, headers: accept_json
      expect(order.reload.status).to eq(Order::DRAFT)
      expect(order.reload.amount).to be_nil
      expect(JSON.parse(response.body)['errors']).to eq({
        'name' => ["can't be blank"],
        'order_details' => ["can't be blank"]
      })
    end
  end
end
