describe CreateOrderDetailForm do
  let(:product) { FactoryGirl.create(:product) }
  let(:order) { Order.new(status: Order::DRAFT) }
  let(:params) do
    {
      product_id: product.id,
      quantity: 2,
      order: order
    }
  end
  describe 'valid' do
    it 'when all params valid' do
      form = described_class.new(params)
      expect(form.valid?).to be true
    end

    it 'invalid when product id nil' do
      params[:product_id] = nil
      form = described_class.new(params)
      expect(form.valid?).to be false
      expect(form.errors[:product]).to eq(["must exist"])
    end

    it 'invalid when product not exists in db' do
      params[:product_id] = '981273981273'
      form = described_class.new(params)
      expect(form.valid?).to be false
      expect(form.errors[:product]).to eq(['must exist'])
    end

    it 'invalid when quantity <= 0' do
      params[:quantity] = -1
      form = described_class.new(params)
      expect(form.valid?).to be false
      expect(form.errors[:quantity]).to eq(['must be greater than 0'])
    end
  end

  describe 'save' do
    it 'create order details' do
      form = described_class.new(params)
      expect(form.save).to eq(true)
      detail = OrderDetail.first
      expect(detail.quantity).to eq(2)
      expect(detail.product).to eq(product)
    end

    it 'create order if order not exist in db' do
      form = described_class.new(params)
      form.save
      order = Order.first
      expect(order.order_details[0]).to eq(form.record)
    end

    it 'dont create order if its already in db' do
      order.save!
      form = described_class.new(params)
      expect { form.save }.to_not change { Order.count}
    end

    it 'failed if invalid' do
      params[:quantity] = -1
      form = described_class.new(params)
      expect(form.save).to eq(false)
      expect(OrderDetail.count).to eq(0)
      expect(Order.count).to eq(0)
    end
  end
end
