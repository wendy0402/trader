describe FinalizeOrderForm do
  let(:product) { FactoryGirl.create(:product, quantity: 4) }
  let(:near_out_of_stock_product) { FactoryGirl.create(:product, quantity: 2) }
  let(:random_product) { FactoryGirl.create(:product, quantity: 2) }

  let(:coupon) { FactoryGirl.create(:coupon) }
  let(:expired_coupon) { FactoryGirl.create(:coupon, :expired) }
  let(:order) { FactoryGirl.create(:order) }
  let(:order_detail) { FactoryGirl.create(:order_detail, order: order, product: product, quantity: 2) }
  let(:order_detail2) { FactoryGirl.create(:order_detail, order: order, product: near_out_of_stock_product) }
  let(:params) do
    {
      order: order,
      coupon_code: coupon.code,
      name: 'Songoku',
      address: 'end of the world',
      email: 'songoku@dragonball.com'
    }
  end

  before do
    order_detail
    order_detail2
  end

  describe 'valid?' do
    it 'valid when params are valid' do
      form = described_class.new(params)
      form.valid?
      expect(form.valid?).to be true
    end

    it 'invalid when any order detail quantity > product quantity' do
      order_detail.update(quantity: 1000)
      form = described_class.new(params)
      expect(form.valid?).to be false
      expect(form.error_messages[:product]).to eq(
        ["#{product.name} not enough stock"]
      )
    end

    it 'invalid when name is nil' do
      params[:name] = ''
      form = described_class.new(params)
      expect(form.valid?).to be false
      expect(form.error_messages[:name]).to eq(["can't be blank"])
    end

    it 'invalid when address is nil' do
      params[:address] = ''
      form = described_class.new(params)
      expect(form.valid?).to be false
      expect(form.error_messages[:address]).to eq(["can't be blank"])
    end

    it 'invalid when email is nil' do
      params[:email] = ''
      form = described_class.new(params)
      expect(form.valid?).to be false
      expect(form.error_messages[:email]).to eq(["can't be blank"])
    end

    it 'invalid when coupon is expired' do
      params[:coupon_code] = expired_coupon.code
      form = described_class.new(params)
      expect(form.valid?).to be false
      expect(form.error_messages[:coupon_code]).to eq(['is not allowed'])
    end

    it 'invalid when coupon does not exist' do
      params[:coupon_code] = 'rand0mpr0bablyN0tExistslah'
      form = described_class.new(params)
      expect(form.valid?).to be false
      expect(form.error_messages[:coupon_code]).to eq(['does not exist'])
    end

    it 'invalid when order status is not draft' do
      order.update(status: Order::SUBMITTED)
      form = described_class.new(params)
      expect(form.valid?).to be false
      expect(form.errors[:status]).to eq(['has been finalized'])
    end
  end

  describe 'save' do
    it 'return true, update status to submitted when success' do
      form = described_class.new(params)
      expect(form.save).to eq(true)
      expect(form.order.status).to eq(Order::SUBMITTED)
    end
    it 'save into database when valid' do
      form = described_class.new(params)
      expect { form.save }.to change {
        order.reload.status
      }.to(Order::SUBMITTED)
      order = Order.first
      expect(order.name).to eq(params[:name])
      expect(order.address).to eq(params[:address])
      expect(order.email).to eq(params[:email])
      expect(order.coupon).to eq(coupon)
    end

    it 'count amount correctly' do
      form = described_class.new(params)
      form.save

      order = Order.first
      normal_price = (product.unit_price * 2) + near_out_of_stock_product.unit_price
      expected_amount = normal_price - coupon.value
      expect(order.amount).to eq(expected_amount)
    end

    it 'deduct the correspond products quantity' do
      form = described_class.new(params)
      expect { form.save }.to change {
        [
          product.reload.quantity,
          near_out_of_stock_product.reload.quantity
        ]
      }.from([4, 2]).to([2, 1])

      first_order_detail = form.order.reload.order_details[0]

      expect(first_order_detail.quantity).to eq(2)
      expect(first_order_detail.amount).to eq(2 * product.unit_price)

      second_order_detail = form.order.reload.order_details[1]

      expect(second_order_detail.quantity).to eq(1)
      expect(second_order_detail.amount).to eq(
        near_out_of_stock_product.unit_price
      )
    end

    it 'deduct quantity coupon' do
      form = described_class.new(params)
      expect { form.save }.to change {
        coupon.reload.quantity
      }.from(10).to(9)
    end

    it 'doesnt deduct wrong product' do
      form = described_class.new(params)
      expect{ form.save }.to_not change {
        random_product.quantity
      }
    end

    it 'use total amount of order details only when no coupon' do
      params[:coupon_code] = nil
      form = described_class.new(params)
      expect(form.save).to be true
      expect(form.order.reload.amount).to eq(
        2 * product.unit_price + near_out_of_stock_product.unit_price
      )
    end

    it 'failed to save when invalid' do
      params[:name] = nil
      form = described_class.new(params)
      expect(form.save).to be false
      expect(order.reload.status).to eq(Order::DRAFT)
      expect(product.reload.quantity).to eq(4)
      expect(near_out_of_stock_product.reload.quantity).to eq(2)
    end
  end
end
