module RequestSupport
  def not_found_response
    { message: 'record not found' }
  end

  def accept_json
    { "ACCEPT" => "application/json" }
  end

  def response_body(data, errors = {})
    default_response_body = { data: data }
    default_response_body[:errors] = errors if errors.present?
    default_response_body
  end
end
