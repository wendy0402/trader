FactoryGirl.define do
  factory :product do
    name { Faker::Commerce.product_name }
    unit_price 120_000
    quantity 3
    description 'this is description'
  end
end
