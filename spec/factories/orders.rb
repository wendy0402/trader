FactoryGirl.define do
  factory :order do
    status Order::DRAFT

    trait :submitted do
      status Order::SUBMITTED
      after :create do |order|
        FactoryGirl.create(:order_detail, product: FactoryGirl.create(:product), order: order)
      end
    end

    trait :paid do
      status Order::PAID
      after :create do |order|
        FactoryGirl.create(:order_detail, product: FactoryGirl.create(:product), order: order)
      end
    end
  end
end
