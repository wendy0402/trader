FactoryGirl.define do
  factory :coupon do
    code { SecureRandom.hex(10) }
    value 10_000
    calculation_type Coupon::FIXED
    start_date 5.days.ago
    end_date 10.days.from_now
    quantity 10

    trait :percentage do
      value 5
      calculation_type Coupon::PERCENTAGE
    end

    trait :expired do
      start_date 5.days.ago
      end_date 2.days.ago
    end
  end
end
