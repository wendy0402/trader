class CancelOrderService
  attr_reader :order

  def initialize(order)
    @order = order
  end

  def execute
    return false unless @order.can_cancel?
    success = true
    Order.transaction do
      begin
        @order.cancel!
        @order.order_details.each do |detail|
          product = detail.product
          product.quantity += detail.quantity
          product.save!
        end
      rescue ActiveRecord::RecordInvalid => e
        Rails.logger.info(e.message)
        Raven.capture_exception(e)
        success = false
        raise e
      end
    end
    success
  end
end
