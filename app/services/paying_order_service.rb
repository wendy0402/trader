class PayingOrderService
  attr_reader :order

  def initialize(order, params = {})
    @params = params.slice(*whitelist_params)
    @order = order
  end

  def execute
    payment = Payment.new(@params.merge(order: @order, paid_at: paid_time))
    can_pay = @order.can_pay?
    if !can_pay || payment.invalid?
      payment.errors.add(:order, "can't be paid") unless can_pay
      return payment
    end

    Payment.transaction do
      payment.save!
      @order.pay!
    end

    payment
  end

  private

  def paid_time
    Time.zone.parse(@params[:paid_at]) if @params[:paid_at].present?
  end

  def whitelist_params
    %i[paid_at account_number account_name account_bank notes]
  end
end
