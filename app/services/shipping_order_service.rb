class ShippingOrderService
  attr_reader :order

  def initialize(order, params = {})
    @order = order
    @params = params.slice(:shipping_id, :logistic_partner_id)
  end

  def execute
    @order.assign_attributes(
      shipping_id: @params[:shipping_id],
      logistic_partner: logistic_partner
    )
    return true if @order.shipping

    # intentionally rename status error message due to
    # unclear message from state machine
    @order.errors.messages.merge!(status: "can't be changed to shipping")
    false
  end

  private

  def logistic_partner
    @logistic_partner ||= LogisticPartner.find_by(@params[:logistic_partner_id])
  end
end
