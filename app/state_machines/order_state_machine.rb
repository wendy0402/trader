class OrderStateMachine < Jsm::Base
  attribute_name :status

  state Order::DRAFT
  state Order::SUBMITTED
  state Order::PAID
  state Order::CANCELLED
  state Order::APPROVED
  state Order::SHIPPING

  event :submit do
    transition from: Order::DRAFT, to: Order::SUBMITTED
  end

  event :pay do
    transition from: Order::SUBMITTED, to: Order::PAID
  end

  event :cancel do
    transition from: Order::PAID, to: Order::CANCELLED
  end

  event :packing do
    transition from: Order::PAID, to: Order::APPROVED
  end

  event :shipping do
    transition from: Order::APPROVED, to: Order::SHIPPING
  end
end
