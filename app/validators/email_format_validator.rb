class EmailFormatValidator < ActiveModel::EachValidator
  attr_reader :validator_value, :validator_attr, :validator_record

  def validate_each(record, attribute, value)
    @validator_record = record
    @validator_value = value
    @validator_attr = attribute
    return true if value.blank?
    check_email_format
  end

  def check_email_format
    return if validator_value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

    validator_record.errors.add(validator_attr, 'must be a valid email address')
  end
end
