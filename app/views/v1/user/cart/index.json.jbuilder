json.array! @order_details do |detail|
  json.partial! 'cart', locals: { order_detail: detail }
end
