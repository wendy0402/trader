json.array! @products do |product|
  json.partial! 'product', locals: { product: product }
end
