if @order_detail.present?
  json.extract! @order_detail, :id, :product_id, :quantity
end
