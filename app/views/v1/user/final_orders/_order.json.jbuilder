json.id order.id
json.email order.email
json.name order.name
json.address order.address
json.amount order.amount

coupon = order.coupon

if coupon.present?
  json.coupon do
    json.id coupon.id
    json.code coupon.code
    json.calculation_type coupon.calculation_type
    json.value coupon.value
  end
end

json.order_details order.order_details do |detail|
  json.product_id detail.product_id
  json.quantity detail.quantity
  json.amount detail.amount
end
