json.id order.id
json.amount order.amount
json.status order.status
json.name order.name
json.phone order.phone
json.email order.email
json.address order.address
json.coupon do
  json.code order.coupon.code
  json.calculation_type order.calculation_type
  json.value order.value
  json.start_date order.start_date
  json.end_date order.end_date
end

json.order_details order.order_details do |detail|
  json.product_id detail.product_id
  json.quantity detail.quantity
  json.amount detail.amount
end

json.created_at order.created_at
json.updated_at order.updated_at
