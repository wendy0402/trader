module V1
  module User
    class CartDetailsController < ApplicationController
      def create
        form = CreateOrderDetailForm.new(create_params.merge(order: current_order))

        if form.save
          @order_detail = form.record
          assign_current_order(@order_detail.order_id)
          render :show, status: :created
        else
          @errors = form.errors
          render :show, status: :unprocessable_entity
        end
      end

      def update
        if order_detail.update_attributes(quantity: params[:quantity])
          render :show, status: :ok
        else
          @errors = order_detail.errors
          render :show, status: :unprocessable_entity
        end
      end

      def destroy
        order_detail.destroy
        head :no_content
      end

      private

      def order_detail
        if current_order.persisted?
          @order_detail ||= current_order.order_details.find(params[:id])
        else
          raise ActiveRecord::RecordNotFound
        end
      end

      def create_params
        params.permit(
          :quantity,
          :product_id
        )
      end
    end
  end
end
