module V1
  module User
    class CartController < ApplicationController
      def index
        @order_details = current_order.order_details
      end
    end
  end
end
