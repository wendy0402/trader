module V1
  module User
    class Orders::PaymentController < ApplicationController
      def create
        service = PayingOrderService.new(order, create_params)
        payment = service.execute

        if payment.errors.blank?
          @payment = payment
          render :show, status: :created
        else
          @errors = payment.errors.messages
          render :show, status: :unprocessable_entity
        end
      end

      private

      def order
        @order ||= Order.find(params[:final_order_id])
      end

      def create_params
        params.permit(:paid_at, :account_number, :account_bank, :account_name)
      end
    end
  end
end
