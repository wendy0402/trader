module V1
  module User
    class FinalOrdersController < ApplicationController
      def create
        order_form = FinalizeOrderForm.new(order_params.merge(order: current_order))
        if order_form.save
          @order = order_form.order
          render :show, status: :created
        else
          @order = order_form.order
          @errors = order_form.error_messages
          render :show, status: :unprocessable_entity
        end
      end

      private

      def order_params
        params.permit(
          :email,
          :name,
          :address,
          :coupon_code
        )
      end
    end
  end
end
