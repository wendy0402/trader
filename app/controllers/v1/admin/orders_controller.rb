module Admin
  class OrdersController < V1::Admin::ApplicationController
    def index
      @orders = non_draft_orders.includes(:payment, :order_details)
    end

    def show
      @order = non_draft_orders.find(params[:id])
    end

    def destroy
      order = non_draft_orders.find(params[:id])
      service = CancelOrderService.new(@order)
      if service.execute
        render :show, status: :no_content
      else
        @errors = { status: ["can't be cancelled"] }
        render :show, status: :unprocessable_entity
      end
    end

    def approve
      order = non_draft_orders.find(params[:id])
      if order.packing
        @order = order
        render :show, status: :ok
      else
        @errors = order.errors.messages.merge(status: ["can't be approved"])
        render :show, status: :unprocessable_entity
      end
    end

    def shipping
      order = non_draft_orders.find(params[:id])
      service = ShippingOrderService.new(order, shipping_params)

      if service.execute
        @order = service.order
        render :show, status: :ok
      else
        @errors = service.order.errors.messages
        render :show, status: :unprocessable_entity
      end
    end

    private

    def non_draft_orders
      @orders ||= Order.non_draft.include(:coupon, :order_details)
    end

    def shipping_params
      params.permit(:shipping_id, :logistic_partner_id)
    end
  end
end
