class ApplicationController < ActionController::API
  include ActionController::ImplicitRender
  include ActionView::Layouts

  rescue_from StandardError, with: :something_went_wrong
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from ActionController::RoutingError, with: :route_not_found

  before_action :set_sentry_context
  layout 'application'

  private

  # @todo: Improve security, dont put order id as it is
  def current_order
    if request.cookies['order_id'].present?
      Order.find(request.cookies['order_id'])
    else
      Order.new(status: Order::DRAFT)
    end
  end

  def assign_current_order(order_id)
    response.set_cookie('order_id', order_id)
  end

  def record_not_found
    error_response(:not_found, 'record not found')
  end

  def route_not_found
    error_response(:not_found, 'route not found')
  end

  def something_went_wrong(exception = nil)
    raise exception if Rails.env.development? || Rails.env.test?
    Raven.capture_exception(exception)
    error_response(500, 'something went wrong')
  end

  def error_response(http_status, message = '')
    render json: { message: message }, status: http_status
  end

  def set_sentry_context
    Raven.extra_context(params: params.to_unsafe_h, url: request.url)
  end
end
