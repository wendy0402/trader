class CreateOrderDetailForm
  include ActiveModel::Model

  attr_accessor :order, :product_id, :quantity

  validate :product_existence
  validate :order_detail_validation
  attr_reader :record

  def save
    return false if invalid?
    order_detail.save!
    @record = order_detail
    true
  rescue ActiveRecord::RecordInvalid
    handle_error([@record])
  end

  private

  def handle_error(record)
    errors.messages.merge!(record.errors.messages)
  end

  def order_validation
    unless order.valid?
      errors.add(:order, order.errors.full_messages.to_sentence)
    end
  end

  def order_detail
    @order_detail ||= OrderDetail.new(
      product: product,
      quantity: quantity,
      order: order
    )
  end

  def order_detail_validation
    return if order_detail.valid?
    handle_error(order_detail)
  end

  def product_existence
    return if product.present?
    errors.add(:product, 'must exist')
  end

  def product
    @product ||= Product.find_by(id: product_id)
  end
end
