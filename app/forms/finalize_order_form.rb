class FinalizeOrderForm
  include ActiveModel::Model
  attr_accessor :order, :coupon_code, :name, :email, :address
  attr_reader :obj
  validates :name, presence: true
  validates :email, presence: true, email_format: true
  validates :address, presence: true
  validates :order, presence: true
  validate :coupon_validation
  validate :order_details_validation
  validate :validate_status

  def save
    return false if invalid?
    order.assign_attributes(
      address: address,
      name: name,
      email: email,
      coupon: coupon,
      amount: order_amount
    )

    Order.transaction do
      if order.invalid?
        order.errors.messages.each do |field, order_errors|
          order_errors.each { |order_error| errors.add(field, order_error) }
        end
        raise ActiveRecord::Rollback
      end

      order.submit!
      update_order_details!
      update_coupon!
    end

    order.persisted?
  end

  def error_messages
    errors.messages
  end

  private

  def validate_status
    errors.add(:status, "has been finalized") unless order.can_submit?
  end

  def order_details
    @order_details ||=
      if order.present?
        order.order_details
      else
        []
      end
  end

  def update_coupon!
    return if coupon.blank?
    coupon.quantity -= 1
    coupon.save!
  end

  def update_order_details!
    order_details.each do |detail|
      detail.order = order
      product = detail.product
      product.quantity -= detail.quantity
      detail.amount = product.unit_price * detail.quantity
      # product use optimistic locking
      product.save!
      detail.save!
    end
  end

  def order_details_validation
    if order_details.blank?
      errors.add(:order_details, "can't be blank")
      return
    end

    order_details.each do |order_detail|
      if order_detail.quantity > order_detail.product.quantity
        errors.add(:product, "#{order_detail.product.name} not enough stock")
      end
    end
  end

  def order_amount
    original_amount = order_details.map do |detail|
      detail.product.unit_price * detail.quantity
    end.inject(&:+)

    coupon.present? ? coupon.discount_result(original_amount) : original_amount
  end

  def coupon
    return @coupon if defined?(@coupon)
    @coupon = Coupon.find_by(code: coupon_code)
  end

  def coupon_validation
    return if coupon_code.blank?
    if coupon.blank?
      errors.add(:coupon_code, 'does not exist')
    elsif !coupon.allowed?
      errors.add(:coupon_code, 'is not allowed')
    end
  end
end
