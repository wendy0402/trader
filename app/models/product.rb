class Product < ApplicationRecord
  has_many :order_details
  validates :quantity, numericality: { greater_than_or_equal_to: 0 }
end
