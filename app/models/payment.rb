class Payment < ApplicationRecord
  belongs_to :order
  validates_presence_of :paid_at,
                        :account_number,
                        :account_bank,
                        :account_name

  validates_length_of :notes, maximum: 4000
end
