class Order < ApplicationRecord
  include Jsm::Client
  include Jsm::Client::ActiveRecord

  belongs_to :coupon, optional: true
  has_many :order_details

  DRAFT = 'draft'.freeze
  SUBMITTED = 'submitted'.freeze
  PAID = 'paid'.freeze
  CANCELLED = 'cancelled'.freeze
  APPROVED = 'approved'.freeze
  SHIPPING = 'shipping'.freeze
  jsm_use OrderStateMachine
  scope :non_draft, -> { where.not(status: Order::DRAFT) }
end
