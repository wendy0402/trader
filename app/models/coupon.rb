class Coupon < ApplicationRecord
  has_many :orders
  PERCENTAGE = 'percentage'.freeze
  FIXED = 'fixed'.freeze

  def allowed?
    (start_date..end_date).cover?(Time.current) && quantity > 0
  end

  def discount_result(amount)
    if calculation_type == PERCENTAGE
      amount - (amount * (value / 100.0))
    else
      amount - value
    end
  end
end
