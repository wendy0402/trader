# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Product.create!(
  name: 'Bottle',
  description: 'Lorem ipsum',
  quantity: 10,
  unit_price: 20_000
)

Product.create!(
  name: 'Teacup',
  description: 'Lorem ipsum',
  quantity: 10,
  unit_price: 50_000
)

Coupon.create!(
  code: SecureRandom.hex(6),
  calculation_type: Coupon::FIXED,
  quantity: 2,
  start_date: 2.days.ago,
  end_date: 2.weeks.from_now
)

LogisticPartner.create!(
  name: 'JNE'
)

LogisticPartner.create!(
  name: 'GOJEK'
)
