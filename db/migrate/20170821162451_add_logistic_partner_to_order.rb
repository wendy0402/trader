class AddLogisticPartnerToOrder < ActiveRecord::Migration[5.1]
  def change
    add_reference :orders, :logistic_partner, foreign_key: true
  end
end
