class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.integer :unit_price
      t.integer :quantity
      t.integer :lock_version

      t.timestamps
    end
  end
end
