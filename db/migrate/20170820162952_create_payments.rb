class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.references :order, foreign_key: true
      t.datetime :transfer_at
      t.string :account_number
      t.string :account_bank
      t.string :account_name
      t.datetime :paid_at
      t.text :notes

      t.timestamps
    end
  end
end
