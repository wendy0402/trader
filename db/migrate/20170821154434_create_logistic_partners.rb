class CreateLogisticPartners < ActiveRecord::Migration[5.1]
  def change
    create_table :logistic_partners do |t|
      t.string :name

      t.timestamps
    end
  end
end
