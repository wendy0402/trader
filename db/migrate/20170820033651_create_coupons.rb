class CreateCoupons < ActiveRecord::Migration[5.1]
  def change
    create_table :coupons do |t|
      t.string :code
      t.datetime :start_date
      t.datetime :end_date
      t.integer :value
      t.integer :quantity
      t.string :calculation_type
      t.integer :lock_version

      t.timestamps
    end
  end
end
