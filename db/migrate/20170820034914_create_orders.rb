class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.float :amount
      t.references :coupon, foreign_key: true
      t.string :status
      t.string :name
      t.string :phone
      t.string :email
      t.string :address
      t.datetime :shipment_at
      t.string :shipping_id

      t.timestamps
    end
  end
end
