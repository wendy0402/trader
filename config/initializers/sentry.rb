Raven.configure do |config|
  config.dsn = Rails.application.secrets.sentry_key.to_s
  config.sanitize_fields = Rails.application
                                .config
                                .filter_parameters
                                .map(&:to_s)
  config.environments = %w[ production ]
  config.logger = Rails.logger
end
