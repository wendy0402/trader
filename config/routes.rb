Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'ping' => 'ping#show'
  namespace :v1, defaults: { format: :json } do
    namespace :admin do
      resources :orders, only: %i[index show destroy] do
        put :approve
        put :shipping
      end
    end
    namespace :user do
      resources :cart, only: %i[index]
      resources :cart_details, only: %i[create update destroy]
      resources :final_orders, only: %i[create] do
        resource :payment, only: %i[create show], controller: 'orders/payment'
      end
    end
  end
end
