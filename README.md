# README

This is a rails application to handle order submission flow. Several assumption that I add here are:
1. User don't need to login when creating an order
2. User don't get email notification on every changes in status
3. No Authentication in admin, since the ultimate focus is transaction flow
4. On cancel, quantity is put back into quantity product
5. no pagination for now
6. order state machine:
    https://chart.googleapis.com/chart?cht=gv&chl=digraph{draft-%3Esubmitted[label=submit];submitted-%3Epaid[label=pay];paid-%3Ecancelled[label=cancel];paid-%3Eapproved[label=packing];approved-%3Eshipping[label=shipping]}

## API

1. cart

    purpose: get list all created order detail endpoint:
```
GET /v1/user/cart

success response:
  status: 200
  body:
  {
    "data": [
      { "id": 1, "product_id": 1, "quantity": 5 },
      { "id": 2, "product_id": 2, "quantity": 2 },
      { "id": 3, "product_id": 1, "quantity": 3 },
    ],
    "errors": {}
}


failed response:
  status: 422
  {
    "data": []
    "errors": {
      "name": ["can't be blank']
    }
  }
```

2. Add to Cart
```
POST /v1/user/cart_details
json body:
  {
    quantity: 10,
    product_id: 5
  }

success response:
  status: 201
  body:
  {
    data: {
      id: 1
      quantity: 10,
      product_id: 5
    },
    errors: {}
  }

failed response:
  status: 422
  body:
  {
    data: {},
    errors: {
      quantity: ["can't be less than 0"]
    }
  }
```

3. delete order detail from cart
```
DELETE /v1/user/cart_details/:id

success response:
  status 200

failed response:
  status 204
```
  

4. Submit Order
purpose: endpoint to finalize orders
endpoint:
```
POST /v1/user/final_orders

json body:

{
  "name": "Joffrey",
  "email": "joff@kingslanding.com",
  "address": "Demangan",
  "coupon_code": "51238" #optional
}

success response:
  status: 200
  body:
    {
      "id": 1,
      "email": "joffrey@kingslanding.com",
      "name": "joffrey",
      "address": "kingslanding castle",
      "amount": 150000,
      "coupon": {
        "id": 1
        "code": 53123 
        "calculation_type": "fixed"
        "value": 10000
      },
      "order_details": [
        {
          "product_id": 10
          "quantity": 1
          "amount": 5000
        },
        {
          "product_id": 11
          "quantity": 2
          "amount": 2500
        },
      ]
    }

failed response:
  status: 422
  body:
    {
      "data": []
      "errors": {
        "name": ["can't be blank']
      }
    }
```

5. pay order

purpose: endpoint to finalize orders
endpoint:
```
POST /v1/user/final_orders/:final_order_id/payment
json body:
{
  "paid_at": "2017-08-22T00:46:31+07:00", #iso8601 format
  "account_name": "jon snow",
  "account_bank": "Nightswatch",
  "account_number": "123456711"
}

success response:
  status: 200
  body:
  {
    data: {
      "paid_at": "2017-08-22T00:46:31+07:00",
      "account_name": "jon snow",
      "account_bank": "Nightswatch",
      "account_number": "123456711"
    }
    errors: {}
  }

  failed response:
    status: 422
    body:
      {
        "data": []
        "errors": {
          "name": ["can't be blank']
        }
      }
```

6. Admin Check all Orders
purpose: endpoint for admin to check all non draft order
endpoint:
```
GET /v1/admin/orders
success response:
  status: 200
  body:
  {
    data: [
      {
        "id": 1,
        "email": "joffrey@kingslanding.com",
        "name": "joffrey",
        "address": "kingslanding castle",
        "status": "approved",
        "amount": 150000,
        "coupon": {
          "id": 1
          "code": 53123 
          "calculation_type": "fixed"
          "value": 10000
        },
        "order_details": [
          {
            "product_id": 10
            "quantity": 1
            "amount": 5000
          },
          {
            "product_id": 11
            "quantity": 2
            "amount": 2500
          },
        ]
      },
      {
        "id": 2,
        "email": "joffrey@kingslanding.com",
        "name": "joffrey",
        "address": "kingslanding castle",
        "status": "submitted",
        "amount": 150000,
        "coupon": {
          "id": 1
          "code": 53123 
          "calculation_type": "fixed"
          "value": 10000
        },
        "order_details": [
          {
            "product_id": 10
            "quantity": 1
            "amount": 5000
          },
          {
            "product_id": 11
            "quantity": 2
            "amount": 2500
          },
        ]
      },
    ],
    errors: {}
  }
```

7. Admin Find order
purpose: endpoint for admin to find a non draft order
endpoint:
```
GET /v1/admin/orders/:id
success response:
  status: 200
  body:
  {
    data: {
      "id": 1,
      "email": "joffrey@kingslanding.com",
      "name": "joffrey",
      "address": "kingslanding castle",
      "status": "approved",
      "amount": 150000,
      "coupon": {
        "id": 1
        "code": 53123 
        "calculation_type": "fixed"
        "value": 10000
      },
      "order_details": [
        {
          "product_id": 10
          "quantity": 1
          "amount": 5000
        },
        {
          "product_id": 11
          "quantity": 2
          "amount": 2500
        },
      ]
    },
    errors: {}
  }
```

8. admin cancel order
purpose: cancel finalized order
```
DELETE /v1/admin/orders/:id

success response:
  status: 204
  json response:
  {
    "data": {},
    "errors": {}
  }
```

9. Admin approved order
purpose: approved paid order
```
PUT /v1/admin/orders/:id/approved

success response:
  status: 200
  json response:
  {
    "data": {
      "id": 1,
      "email": "joffrey@kingslanding.com",
      "name": "joffrey",
      "address": "kingslanding castle",
      "status": "approved",
      "amount": 150000,
      "coupon": {
        "id": 1
        "code": 53123 
        "calculation_type": "fixed"
        "value": 10000
      },
      "order_details": [
        {
          "product_id": 10
          "quantity": 1
          "amount": 5000
        },
        {
          "product_id": 11
          "quantity": 2
          "amount": 2500
        },
      ]
    },
    "errors":{}
  }

  failed response:
  status: 422
  json response:
  {
    body: {
      "data": []
      "errors": {
        "name": ["can't be blank']
      }
    }
  }
  
```

7. Shipped Order
purpose: mark order as shipped
success response:
  status: 200
  json response:
  {
    "data": {
      "id": 1,
      "email": "joffrey@kingslanding.com",
      "name": "joffrey",
      "address": "kingslanding castle",
      "status": "approved",
      "amount": 150000,
      "coupon": {
        "id": 1
        "code": 53123 
        "calculation_type": "fixed"
        "value": 10000
      },
      "order_details": [
        {
          "product_id": 10
          "quantity": 1
          "amount": 5000
        },
        {
          "product_id": 11
          "quantity": 2
          "amount": 2500
        },
      ]
    },
    "errors":{}
  }
```
